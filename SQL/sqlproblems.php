<?php
    $db_host = "localhost";
    $db_user = "root";
    $db_pass = "";
    $db_name = "companytest";

    try {
        $db_conn = new PDO("mysql:host={$db_host};dbname={$db_name}",$db_user,$db_pass);
        $db_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
        catch(PDOException $e) {
            echo $e->getMessage();
    }

    $a = $db_conn->prepare("SELECT * FROM `employees` WHERE last_name LIKE 'K%'");
    $a->execute();

    $b = $db_conn->prepare("SELECT * FROM `employees` WHERE last_name LIKE '%i'");
    $b->execute();

    $c = $db_conn->prepare("SELECT CONCAT ( first_name, ' ' , middle_name, ' ', last_name ) AS Result, hire_date FROM employees WHERE hire_date >= '2015-01-01' AND hire_date <= '2015-03-31' ORDER BY hire_date ASC;");
    $c->execute();

    $d = $db_conn->prepare("SELECT lne.last_name AS 'last_name_emp', lnb.last_name AS 'last_name_boss' FROM employees AS lnb INNER JOIN employees AS lne ON lnb.id = lne.boss_id WHERE lne.last_name IS NOT NULL;");
    $d->execute();

    $e = $db_conn->prepare("SELECT last_name FROM employees WHERE department_id = 3 ORDER BY department_id DESC");
    $e->execute();

    $f = $db_conn->prepare("SELECT COUNT(middle_name) as count_has_middle FROM employees;");
    $f->execute();

    $g = $db_conn->prepare("SELECT count(*) AS count, dept.name AS 'dept_name' FROM employees AS emp INNER JOIN departments AS dept ON emp.department_id = dept.id GROUP BY dept.name ORDER BY dept.name ASC");
    $g->execute();

    $h = $db_conn->prepare("SELECT CONCAT ( first_name, ' ' , middle_name, ' ', last_name ) AS Result,hire_date FROM employees where hire_date = (SELECT max(hire_date) FROM employees)");
    $h->execute();

    $i = $db_conn->prepare("SELECT name AS 'dept_name' FROM departments AS dept WHERE dept.name not IN(SELECT dept.name FROM employees AS emp INNER JOIN departments AS dept ON emp.department_id = dept.id)");
    $i->execute();

    $j = $db_conn->prepare("SELECT CONCAT_WS(last_name, ' ', first_name) AS 'emp_name' FROM employees AS emp, positions AS pos INNER JOIN employee_positions AS epos ON pos.id = epos.position_id HAVING count(epos.employee_id) > 2");
    $j->execute();

?>
<!DOCTYPE html>
<html>
<head>
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <table>
                <h1>Problem #1</h1>
                <thead>
                    <tr>
                        <th>Id Number</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Middle Name</th>
                        <th>Birth Date</th>
                        <th>Hire Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while($row = $a->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                            <tr>
                                <td><?= $row["id"]; ?></td>
                                <td><?= $row["first_name"]; ?></td>
                                <td><?= $row["last_name"]; ?></td>
                                <td><?= $row["middle_name"]; ?></td>
                                <td><?= $row["birth_date"]; ?></td>
                                <td><?= $row["hire_date"]; ?></td>
                            </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <table>
                <h1>Problem #2</h1>
                <thead>
                    <tr>
                        <th>Id Number</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Middle Name</th>
                        <th>Birth Date</th>
                        <th>Hire Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while($row = $b->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                            <tr>
                                <td><?= $row["id"]; ?></td>
                                <td><?= $row["first_name"]; ?></td>
                                <td><?= $row["last_name"]; ?></td>
                                <td><?= $row["middle_name"]; ?></td>
                                <td><?= $row["birth_date"]; ?></td>
                                <td><?= $row["hire_date"]; ?></td>
                            </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <table>
                <h1>Problem #4</h1>
                <thead>
                    <tr>
                        <th>Employee Last Name</th>
                        <th>Boss Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while($row = $d->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                            <tr>
                                <td><?= $row["last_name_emp"]; ?></td>
                                <td><?= $row["last_name_boss"]; ?></td>
                            </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <table>
                <h1>Problem #3</h1>
                <thead>
                    <tr>
                        <th>Full Name</th>
                        <th>Hire Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while($row = $c->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                            <tr>
                                <td><?= $row["Result"]; ?></td>
                                <td><?= $row["hire_date"]; ?></td>
                            </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <table>
                <h1>Problem #5</h1>
                <thead>
                    <tr>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while($row = $e->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                            <tr>
                                <td><?= $row["last_name"]; ?></td>
                            </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <table>
                <h1>Problem #6</h1>
                <thead>
                    <tr>
                        <th>Count</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while($row = $f->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                            <tr>
                                <td><?= $row["count_has_middle"]; ?></td>
                            </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <table>
                <h1>Problem #7</h1>
                <thead>
                    <tr>
                        <th>Department Name</th>
                        <th>Count</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while($row = $g->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                            <tr>
                                <td><?= $row["dept_name"]; ?></td>
                                <td><?= $row["count"]; ?></td>
                            </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <table>
                <h1>Problem #8</h1>
                <thead>
                    <tr>
                        <th>Employee Full Name</th>
                        <th>Hire Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while($row = $h->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                            <tr>
                                <td><?= $row["Result"]; ?></td>
                                <td><?= $row["hire_date"]; ?></td>
                            </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <table>
                <h1>Problem #9</h1>
                <thead>
                    <tr>
                        <th>Department Name</th>
                        <th>Count</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while($row = $i->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                            <tr>
                                <td><?= $row["dept_name"]; ?></td>
                                <td></td>
                            </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <table>
                <h1>Problem #10</h1>
                <thead>
                    <tr>
                        <th>Full name</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while($row = $j->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                            <tr>
                                <td><?= $row["emp_name"]; ?></td>
                            </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>