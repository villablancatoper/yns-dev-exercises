<?php
    $db_host = "localhost";
    $db_user = "root";
    $db_pass = "";
    $db_name = "validate";

    try {
        $db_conn = new PDO("mysql:host={$db_host};dbname={$db_name}",$db_user,$db_pass);
        $db_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
        catch(PDOException $e) {
            echo $e->getMessage();
    }

    $query = $db_conn->prepare("SELECT * FROM users");
    $query->execute();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <table class="table">
                <h1>List of employees</h1>
                <thead>
                    <tr>
                        <th>Id Number</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Created at</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while($row = $query->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                            <tr>
                                <td><?= $row["id"]; ?></td>
                                <td><?= $row["username"]; ?></td>
                                <td><?= $row["password"]; ?></td>
                                <td><?= $row["created_at"]; ?></td>
                            </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>