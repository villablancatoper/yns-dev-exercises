<!DOCTYPE html>
<html>
<head>
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
    <select name="imageSelect" id="imageSelect">
        <option value="Rakdos_Logo.png">Rakdos</option>
        <option value="Golgari_Logo.png">Golgari</option>
        <option value="Simic_Logo.png">Simic</option>
        <option value="Izzet_Logo.png">Izzet</option>
    </select><br/><br/>
    <img src="Rakdos_Logo.png" id="imgChange" name="imgChange" height="250px" width="250px">
</body>
<script>
    $("#imageSelect").on('change' ,function(){
        let location = $(this).val();
        $('img').attr('src', location);
    });
</script>
</html>