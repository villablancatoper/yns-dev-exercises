<!DOCTYPE html>
<html>
<head>
    <title>Document</title>
</head>
<body>
    <label id="fn"> Please enter First number : </label>
    <input type="number" name="firstNumber" id="firstNumber"><br />
    <label id="sn"> Please enter Second number : </label>
    <input type="number" name="secondNumber" id="secondNumber"><br />
    <label id="op"> Please enter the operation : </label>
    <select name="opeation" id="operation">
        <option value="add"> + </option>
        <option value="minus"> - </option>
        <option value="multiply"> * </option>
        <option value="divide"> / </option>
    </select><br>
    <label id="answer"></label><br />
    <button id="compute" onclick="Calculate()">Show Answer</button>
</body>
<script>
        function Calculate(){
            var operation = document.getElementById("operation").value;
            var var1 = parseInt(document.getElementById("firstNumber").value);
            var var2 = parseInt(document.getElementById("secondNumber").value);
            var answer;

            switch(operation){
                case "add":
                    answer = var1 + var2;
                    break; 
                case "minus":
                    answer = var1 - var2;
                    break;
                case "multiply":
                    answer = var1 * var2;
                    break;
                case "divide":
                    answer = var1 / var2    ;
                    break;
                default:
                    break;
            }
            document.getElementById("answer").innerHTML = answer
        }
</script>
</html>