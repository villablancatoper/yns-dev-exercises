<!DOCTYPE html>
<html>
<head>
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
    <button id="small">Small</button><br />
    <button id="medium">Medium</button><br />
    <button id="large">Large</button><br />
    <br/><br/>
    <img id="resize" src="Rakdos_Logo.png" height="100px" width="100px">
</body>
<script>
var image = document.getElementById('resize');
$("#small").click(function(){
    image.style.height = '100px';
    image.style.width = '100px';
});
$("#medium").click(function(){
    image.style.height = '200px';
    image.style.width = '200px';
});
$("#large").click(function(){
    image.style.height = '300px';
    image.style.width = '300px';
});
</script>
</html>