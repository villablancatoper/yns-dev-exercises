<!DOCTYPE html>
<html>
<head>
    <title>Document</title>
    <style>
        body {
            animation: colorchange 10s; /* animation-name followed by duration in seconds*/
            /* you could also use milliseconds (ms) or something like 2.5s */
            -webkit-animation: colorchange 10s; /* Chrome and Safari */
        }
        @keyframes colorchange{
            0%   {background: red;}
            100% {background: lightBlue;}
        }
        @-webkit-keyframes colorchange /* Safari and Chrome - necessary duplicate */{
            0%   {background: green;}
            100% {background: lightGreen;}
        }
    </style>
</head>
<body>
    <h1>BG Change</h1>
</body>
</html>