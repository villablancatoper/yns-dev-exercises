<?php
    $host = "localhost";
    $username = "root";
    $password = "";
    $database = "db_quiz";

    $db_conn = mysqli_connect($host, $username, $password, $database);

    if ($db_conn-> connect_error){
        die("Connection Failed: ". $db_conn-> connect_error); 
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<body>
    <div class="container-fluid">
        <div class="box">
            <form method="post">

                <h2> Guess the next line </h2>
                <?php
                $questionArray = 'SELECT * FROM questionSet ORDER BY rand()';
                $questionResult = mysqli_query($db_conn, $questionArray);
                $qNum = 1;

                while($row = mysqli_fetch_array($questionResult)){
                    echo"<span> Question Number " .$qNum. ": " .$row["question"]. "<br>";
                    $number = $row["id"];

                    $newAnswer = "SELECT * FROM answerSet WHERE id = '$number'";
                    $resultAnswer = mysqli_query($db_conn, $newAnswer);

                    while($ans = mysqli_fetch_array($resultAnswer)){
                        echo "<input type='radio' value=''" .$ans["answer"]. "name='choice[" .$qNum. "]'>" .$ans["answer"]. "</input><br>";
                    }
                    $qNum++;
                }
                echo"<br>";
                echo"<input type='submit' name='submit'>";
                ?>
            </form>
        </div>
    </div>    
</body>
</html>