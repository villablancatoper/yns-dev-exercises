<?php
    if (!isset($_REQUEST["month"])) {
        $_REQUEST["month"] = date("n");
    } 

    if (!isset($_REQUEST["year"])) {
        $_REQUEST["year"] = date("Y");
    }

    $monthNames = Array("January", "February", "March", "April", "May", "June", "July", 
    "August", "September", "October", "November", "December");

    $monthNow = $_REQUEST["month"];
    $yearNow = $_REQUEST["year"];

    $previousYear = $yearNow;
    $nextYear = $yearNow;
    $previousMonth = $monthNow - 1;
    $nextMonth = $monthNow + 1;

    if ($previousMonth == 0 ) {
        $previousMonth = 12;
        $previousYear = $yearNow - 1;
    }

    if ($nextMonth == 13 ) {
        $nextMonth = 1;
        $nextYear = $yearNow + 1;
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<body>
    <div class="header">
        <h2><?= $monthNames[$monthNow - 1]. " " .$yearNow; ?></h2>
    </div>
    <table class="table-bordered">
        <thead>
            <tr>
                <th>Sunday</th>
                <th>Monday</th>
                <th>Tuesday</th>
                <th>Wednesday</th>
                <th>Thursday</th>
                <th>Friday</th>
                <th>Saturday</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $timeStamp = mktime(0, 0, 0, $monthNow, 1, $yearNow);
            $endDay = date("t", $timeStamp);
            $thisMonth = getdate ($timeStamp);
            $startDay = $thisMonth['wday'];
            $dateNow = date("d");
            $currentMonth = date("n");
            for ($i = 0; $i < ($endDay + $startDay); $i++) {
                if (($i % 7) == 0 ) {
                    echo "<tr>";
                }
                if ($i < $startDay) {
                    echo "<td></td>";
                } else {
                    $listDate = $i - $startDay + 1;
                    if(($listDate ==  $dateNow) && ($monthNames[$monthNow - 1] == $monthNames[$currentMonth -1])) {
                        echo "<td class='currentDate'>" .$listDate. "</td>";
                    } else {
                        echo "<td class='date'>" .$listDate. "</td>";
                    }
                }
                if (($i % 7) == 6 ) {
                    echo "</tr>";
                }
            }
        ?>
        </tbody>
    </table>
    <div class="navigation">
        <button type="button" name="previous">
            <a href="<?= $_SERVER["PHP_SELF"] . "?month=". $previousMonth . "&year=" . $previousYear; ?>">
            Previous</a>
        </button>
        <button type="button" name="next">
            <a href="<?= $_SERVER["PHP_SELF"] . "?month=". $nextMonth . "&year=" . $nextYear; ?>">
            Next</a>
        </button>
    </div>
</body>
</html>