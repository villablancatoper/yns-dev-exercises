<!DOCTYPE html>
<html>
<head>
    <title>Document</title>
</head>
<body>

    <h2> Villablanca, Christopher P.</h2>
    <hr>
    <a href="HTML and PHP/1-1.php">1-1 Show Hello World</a><br>
    <a href="HTML and PHP/1-2.php">1-2 Basic Arithmetic</a><br>
    <a href="HTML and PHP/1-3.php">1-3 GCD</a><br>
    <a href="HTML and PHP/1-4.php">1-4 Fizzbuz</a><br>
    <a href="HTML and PHP/1-5.php">1-5 Input date. Then show 3 days from inputted date and its day of the week</a><br>
    <a href="HTML and PHP/1-6.php">1-6 Input user information. Then show it in next page</a><br>
    <a href="HTML and PHP/1-6.2.php">1-6.2 Input user information. Then show it in next page</a><br>
    <a href="HTML and PHP/1-6.php">1-6 Add validation in the user information form(required, numeric, character, mailaddress)</a><br>
    <a href="HTML and PHP/1-8.php">1-8 Store inputted user information into a CSV file./a><br>
    <a href="HTML and PHP/1-9.php">1-9 Show the user information using table tags</a><br>
    <a href="HTML and PHP/1-10.php">1-10 Upload images</a><br>
    <a href="HTML and PHP/1-11.php">1-11 Show uploaded images in the table</a><br>
    <a href="HTML and PHP/1-12.php">1-12 Add pagination in the list page</a><br>
    <a href="HTML and PHP/1-13.php">1-13 Create login form and embed it into the system that you developed</a><br>
    <a href="HTML and PHP/1-13_Registered.php">1-13 Registration</a><br>
    <a href="HTML and PHP/1-13_RegisteredInfo.php">1-13 Registered Information</a><br>
    <hr>
    <a href="Javascript/2-1.html">2-1 Show Alert</a><br>
    <a href="Javascript/2-2.html">2-2 Confirm dialog and redirection</a><br>
    <a href="Javascript/2-3.html">2-3 The four basic operations of arithmetic</a><br>
    <a href="Javascript/2-4.html">2-4 Show prime numbers</a><br>
    <a href="Javascript/2-5.html">2-5 Input characters in text box and show it in label</a><br>
    <a href="Javascript/2-6.html">2-6 Press button and add a label below button</a><br>
    <a href="Javascript/2-7.html">2-7 Show alert when you click an image</a><br>
    <a href="Javascript/2-8.html">2-8 Show alert when you click link</a><br>
    <a href="Javascript/2-9.html">2-9 Change text and background color when you press buttons</a><br>
    <a href="Javascript/2-10.html">2-10 Scroll screen when you press buttons</a><br>
    <a href="Javascript/2-11.html">2-11 Change background color using animation</a><br>
    <a href="Javascript/2-12.html">2-12 Show another image when you mouse over an image. Then show the original image when you mouse out.</a><br>
    <a href="Javascript/2-13.html">2-13 Change size of images when you press buttons</a><br>
    <a href="Javascript/2-14.html">2-14 Show images according to the options in combo box</a><br>
    <a href="Javascript/2-15.html">2-15 Show current date and time in real time</a><br>
    <hr>
    <a href="5-1.php">5-1 Quiz with three multiple choices</a><br>
    <a href="5-2.html">5-2 Calendar</a><br>
</body>
</html>