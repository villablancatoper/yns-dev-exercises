<!DOCTYPE html>
<html>
<head>
    <title>Document/title>
</head>
<body>
    <form method="POST">
        Firstname: <input type="text" name="firstName" REQUIRED><br/>
        Email: <input type="email" name="email" REQUIRED><br/>
        Picture <input type="file" name="image" REQUIRED><br/>
        <input type="submit" name="submit">
    </form>
</body>
<?php
    if (array_key_exists('submit', $_POST)){
        $firstName = $_POST['firstName'];
        $email = $_POST['email'];
        $inputValues = '';

        $imgExt = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);

        $isImage = false;

        $validImgExt = array("JPG","JPEG","GIF","JFIF","PNG");

        foreach ($validImgExt as $ext){
            if ($ext == strtoupper(($imgExt))){
                $isImage = true;
            }
        }
        
        if (!$isImage)
        {
            echo "Failed to save your data. <br/>The file you submitted is not an image.";
        }
        else{
            $date = date('Ymdhis');
            $target_dir = "";
            $target_file = $target_dir.$date.basename($_FILES["image"]["name"]);
            
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

            move_uploaded_file( $_FILES['image']['tmp_name'], $target_file);

            if (!file_exists('1-10.csv')){
                $csvFile = fopen("names.csv","w");
            }

            $csvFile = fopen("names.csv", "a");
            if (count(file('names.csv')) !== 0){
                $inputValues = "\n";
            }
            $inputValues .= $target_file.",".$firstName.",".$email.",";

            if ($csvFile){
                fwrite($csvFile,$inputValues);
                fclose($csvFile);
                echo "Data Stored to the CSV.";
            }
        }
    }
?>
</html>
