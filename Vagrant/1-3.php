<?php
    if (isset($_POST["submit"])) {

        $total1 = $_POST["gcd1"];
        $total2 = $_POST["gcd2"];

        echo "Greatest Common Divisor of ". $total1 . " and " . $total2 . " is " . getGCF($total1, $total2);

    } 
    function getGCF($x, $y) {
        if ($x == 0 && $y == 0) {
            return 0;
        }
        if ($x == 0) {
            return $y;
        }
        if ($x > $y) {
            $x = $x - $y;
            return $y - $x;
        }
            return $y - $x;
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form method="POST" action="<?= htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        First Number = <input type="number" name="gcd1">
        <br />
        Second Number = <input type="number" name="gcd2">
        <br />
        <input type="submit" value="submit" name="submit">
    </form>
    
</body>
</html>