<?php
        if (isset($_POST["submit"])) {

            $myDate = $_POST["dateNow"];
            echo "The date today is " . $myDate;
            $myDate2 = date('Y-m-d', strtotime($myDate . ' +1 day'));
            echo "<br>The date tomorrow is " . $myDate2;
            $myDate3 = date('Y-m-d', strtotime($myDate . ' +2 days'));
            echo "<br>The next date is " . $myDate3;
            $myDate4 = date('Y-m-d', strtotime($myDate . ' +3 days'));
            echo "<br>The next date is " . $myDate4;
        } 

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <form method="POST" action="<?= htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <label> Please enter the date </label><br>
        <input type="date" name="dateNow">
        <br />
        <input type="submit" value="submit" name="submit">
    </form>

</body>
</html>