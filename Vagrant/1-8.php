<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<?php
        $error = $email = $name = "";

        function cleanInput($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $name = $_POST["name"];
            $email = $_POST["email"];

            // Validate name
            if (!empty($name)) {
                if (preg_match("/^[a-zA-Z\.]+$/", $name)) {
                    $name = cleanInput($name);
                } else {
                    $error .= "Please insert text only for name<br />";
                }
            } else {
                $error .= "Please insert a name<br />";
            }
            // Validate email
            if (!empty($email)) {
                $emailValidation = "/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/";
                if (preg_match($emailValidation, $email)) {
                    $email = cleanInput($email);
                } else {
                    echo "Invalid Email Address<br />";
                }
            } else {
                $error .= "Please insert an email<br />";
            }

            if ($error == "") {
                $fileOpen = fopen("names.csv","a");
                $formData = array(
                    "name"  => $name,
                    "email" => $email
                );
                fputcsv($fileOpen, $formData);
                echo "</i>Data saved and imported</i>";
                $name = "";
                $email = "";
            }          
        }
    ?>
    <form action = "<?= htmlspecialchars($_SERVER["PHP_SELF"]);?> " method="POST">
        <?= $error; ?>
        <h2>Please enter user info</h2>
        <label for="name"> Name </label><br />
        <input type="text" name="name" value="<?= $name; ?>">
        <br>
        <label for="email"> Email </label><br />
        <input type="text" name="email" value="<?= $email; ?>">
        <br>
        <input type="submit" value="submit" name="submit">
    </form>
</body>
</html>
