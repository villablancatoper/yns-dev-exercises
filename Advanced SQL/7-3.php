<?php
    $db_host = "localhost";
    $db_user = "root";
    $db_pass = "";
    $db_name = "companytest";

    try {
        $db_conn = new PDO("mysql:host={$db_host};dbname={$db_name}",$db_user,$db_pass);
        $db_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e) {
        echo $e->getMessage();
    }

    $query = $db_conn->prepare("SELECT `emp`.*, 
        (CASE `pos`.`name`
            WHEN 'CEO' THEN 'Chief Executive Officer'
            WHEN 'CTO' THEN 'Chief Technical Officer'
            WHEN 'CFO' THEN 'Chief Financial Officer'
            ELSE `pos`.`name` 
            END) AS curr_pos from employees emp
        INNER JOIN employee_positions ep ON `ep`.`employee_id` = `emp`.`id`
        INNER JOIN positions pos ON `ep`.`position_id` = `pos`.`id`");
    $query->execute();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <table>
                    <h2>Employee Positions</h2>
                    <thead>
                        <tr>
                            <th>Id Number</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Middle Name</th>
                            <th>Birth Date</th>
                            <th>Hire Date</th>
                            <th>Position</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            while($row = $query->fetch(PDO::FETCH_ASSOC)) {
                            ?>
                                <tr>
                                    <td><?= $row["id"]; ?></td>
                                    <td><?= $row["first_name"]; ?></td>
                                    <td><?= $row["last_name"]; ?></td>
                                    <td><?= $row["middle_name"]; ?></td>
                                    <td><?= $row["birth_date"]; ?></td>
                                    <td><?= $row["hire_date"]; ?></td>
                                    <td><?= $row["curr_pos"]; ?></td>
                                </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>