<?php
    $db_host = "localhost";
    $db_user = "root";
    $db_pass = "";
    $db_name = "validate";

    try {
        $db_conn = new PDO("mysql:host={$db_host};dbname={$db_name}",$db_user,$db_pass);
        $db_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
        catch(PDOException $e) {
            echo $e->getMessage();
    }

    $a = $db_conn->prepare("SELECT * FROM `sevenfour`");
    $a->execute();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
</head>
<body>
<div class="row">
            <table>
                <h1>Problems</h1>
                <thead>
                    <tr>
                        <th>Full name</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while($row = $a->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                            <tr>
                                <td><?= $row["id"]; ?></td>
                                <td><?= $row["parent_id"]; ?></td>
                            </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>
</body>
</html>