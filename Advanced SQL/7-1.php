<?php
    $db_host = "localhost";
    $db_user = "root";
    $db_pass = "";
    $db_name = "companytest";

    try {
        $db_conn = new PDO("mysql:host={$db_host};dbname={$db_name}",$db_user,$db_pass);
        $db_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
        catch(PDOException $e) {
            echo $e->getMessage();
    }

    $query = $db_conn->prepare("SELECT *, FLOOR(DATEDIFF(NOW(), birth_date )/365) as Age  FROM employees WHERE FLOOR(DATEDIFF(NOW(), birth_date ) / 365) BETWEEN 40 AND 50");
    $query->execute();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <table>
                <h1>List of employees</h1>
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Middle Name</th>
                        <th>Birth Date</th>
                        <th>Hire Date</th>
                        <th>Age</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while($row = $query->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                            <tr>
                                <td><?= $row["id"]; ?></td>
                                <td><?= $row["first_name"]; ?></td>
                                <td><?= $row["last_name"]; ?></td>
                                <td><?= $row["middle_name"]; ?></td>
                                <td><?= $row["birth_date"]; ?></td>
                                <td><?= $row["hire_date"]; ?></td>
                                <td><?= $row["Age"]; ?></td>
                            </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>