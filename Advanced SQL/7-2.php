<?php
    $db_host = "localhost";
    $db_user = "root";
    $db_pass = "";
    $db_name = "validate";

    try {
        $db_conn = new PDO("mysql:host={$db_host};dbname={$db_name}",$db_user,$db_pass);
        $db_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e) {
        echo $e->getMessage();
    }
    
    $query = $db_conn->prepare("SELECT * FROM `daily_work_shifts` INNER JOIN therapists ON 
    `therapists`.`id` = `daily_work_shifts`.`therapists_id` ORDER BY 
    (CASE 
        WHEN start_time <= '05:59:59' AND start_time >='00:00:00' THEN start_time
        ELSE target_date + 1
    END)");
    $query->execute();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<body>
    <div class="container-fluid">
        <div class="box">
        <table class="table">
        <h1>Schedule of Therapists</h1>
        <thead>
            <tr>
                <th>Name</th>
                <th>Target Date</th>
                <th>Start time</th>
                <th>End Time</th>
            </tr>
        </thead>
        <tbody>
            <?php
                while($row = $query->fetch(PDO::FETCH_ASSOC)) {
            ?>
                    <tr>
                        <td><?= $row["name"]; ?></td>
                        <td><?= $row["target_date"]; ?></td>
                        <td><?= $row["start_time"]; ?></td>
                        <td><?= $row["end_time"]; ?></td>
                    </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
        </div>
    </div>    
</body>
</html>