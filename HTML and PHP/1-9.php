<?php
    $openFile = fopen("names.csv", "r") or die ("Cannot open the file");
    $fp = file("names.csv");
    $numOfRows = count($fp);
    $start = 1;
    $end = 10;
    $url = $_SERVER["REQUEST_URI"];
    if (isset($_GET["page"])) {
        // Get page number
        $currentPage = $_GET["page"];
        $perPage = 10;
        // Get the start of the data to display
        $start = (($currentPage * $perPage) - $perPage) + 1;
        // Get the end of the data to display
        $end = $start + ($perPage - 1);
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Imeji</title>
</head>
<body>
    <table>
        <h2>Information</h2>
        <thead>
            <tr>
                <th></th>
                <th>Name</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>
            <?php
                while (($data = fgetcsv($openFile, 1000, ",")) !== false) {
                    $num = count($data);
                    $totalPages = ceil ($numOfRows / 10);
                    $csv[] =  $data;
                    // Check the presence of the data
                    if ($num > 0) {
                        if ($start <= $end) { 
                            echo "<tr>";
                            echo "<td> * </td>";
                            for ($i = 0; $i < $num; $i++) {
                                echo "<td>" . $data[$i] . "</td>";
                            }
                            $start++;
                        }
                    } else {
                        echo "No Data Available";
                    }
                    echo "</tr>";
                }
                fclose($openFile);
            ?>
        </tbody>
    </table>
</body>
</html>